
 I read NoSQL for Dummies and learned that there's a whole job balancing availability, consistency, organization of data. Also, we may need some data to be secure, cached and in JSON. This biased post https://medium.com/@redgeoff/introducing-deltadb-finally-there-is-hope-for-the-write-once-run-everywhere-html5-app-afd5c59baa5b recommends a new structure that claims to be better than Firebase, PouchDB and Meteor.

	I don't know what to choose and don't want to be stuck with local storage and a slow database. Any thoughts?