
How about a cursor instead of an avatar for interactions.
- until we can move around a static humanoid

How about a counter instead of an inventory GUI.
- until we can create and destroy simple spheres

How about a demonstration with a tree that creates spheres IF a player is within 3 units
- until there is glTF procedural generation of colors, etc.